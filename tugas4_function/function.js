//1
function teriak(){
    var name = "Halo Sanbers!";
    return name
}
console.log(teriak())

// 2
function kalikan(num1, num2){
    return num1*num2
}
var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2)
console.log(hasilKali)

//3
function introduce(name, age, address, hobby){
    return 'Nama saya '+name+', Umur saya '+age+ ' tahun, Alamat saya di '+address+ ', dan saya punya hobby yaitu '+hobby+'!';
}    
    var name = "Fahmi";
    var age = 22;
    var address = "Jln. Soepomo, Yogyakarta";
    var hobby = "Futsal";
    var perkenalan = introduce(name, age, address, hobby);
    console.log(perkenalan)